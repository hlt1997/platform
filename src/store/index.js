/* vuex 状态管理 用来存储全局变量 */
import Vue from 'vue'
import Vuex from 'vuex'
import * as utils from '../utils/index.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  getters: {

  },
  actions: {

  },
  mutations: {

  },
  modules: {
    user: {
      state: {
        userId: utils.storage.get('userId') || '',
        token: utils.storage.get('token') || '', // 登录加密字符串
        userInfo: utils.storage.get('userInfo') || {},
        userStatus: true, // 游客状态
        userRank: 0, // 身份等级 0 普通用户 1 学生 2 设计师 3 企业 4 园区
        collegesPicture: utils.storage.get('collegesPicture') || {} // 全局logo
      },
      getters: {
        getUserStatus (state) {
          return state.userStatus
        }
        // getUserRank (state, data) {
        //   return state.userRank
        // }
      },
      actions: {

      },
      mutations: {
        setUserId (state, data) {
          state.userId = data
          utils.storage.set('userId', data)
        },
        setToken (state, data) {
          state.token = data
          utils.storage.set('token', data)
        },
        setUserInfo (state, data) {
          state.userInfo = data
          utils.storage.set('userInfo', data)
        },
        setSchoolLogo (state, data) {
          state.collegesPicture = data
          utils.storage.set('collegesPicture', data)
        },
        setUserStatus (state, data) {
          state.userStatus = data
        },
        setUserRank (state, data) {
          state.userRank = data
        },
        removeUserId (state) {
          state.userId = ''
          utils.storage.remove('userId')
        },
        removeToken (state) {
          state.token = ''
          utils.storage.remove('token')
        },
        removeUserInfo (state) {
          state.userInfo = ''
          utils.storage.remove('userInfo')
        }
        // setStateData (state, data) {
        //   state[data.name] = data.params
        //   utils.storage.set(data.name, data.params)
        // }
      }
    },
    global: {
      state: {
        globalScrollMark: []
      },
      getters: {

      },
      actions: {

      },
      mutations: {
        pushScroll (state, data) {
          state.globalScrollMark[data.pathName] = {
            scrollTop: data.scrollTop,
            scrollLeft: data.scrollLeft
          }
        },
        setScroll () {
          document.getElementById('vux_view_box_body').scrollTop = 0
          document.getElementById('vux_view_box_body').scrollLeft = 0
        }
      }
    }
  }
})
