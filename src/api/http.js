/* 封装axios请求方法 */
import Vue from 'vue'
import axios from 'axios'
import store from '../store'
import router from '../router'
import * as utils from '../utils'
import qs from 'qs'

axios.defaults.timeout = 5000
let baseApiUrl = window.location.host
if (baseApiUrl.indexOf('192.168') !== -1 || baseApiUrl.indexOf('localhost') !== -1) {
  // baseApiUrl = 'http://www.artcreative.cn/cxpt'
  // baseApiUrl = 'http://192.168.1.77:8884/cxpt'
  // baseApiUrl = 'http://192.168.1.194:8884/cxpt'
  baseApiUrl = 'http://192.168.1.112:8884/cxpt'
} else {
  baseApiUrl = 'http://' + window.location.host + '/cxpt'
}

axios.defaults.baseURL = baseApiUrl

let cancel = {}
let promiseArr = {}
const CancelToken = axios.CancelToken

// http request 拦截器1
axios.interceptors.request.use(
  config => {
    // 发起请求时，取消掉当前正在进行的相同请求
    if (promiseArr[config.url]) {
      promiseArr[config.url]('操作取消')
      promiseArr[config.url] = cancel
    } else {
      promiseArr[config.url] = cancel
    }
    const token = utils.storage.get('token')
    if (token) {
      config.headers.token = token
    }
    return config
  },
  err => {
    return Promise.reject(err)
  }
)

// http response 拦截器
axios.interceptors.response.use(
  response => {
    response = response.data
    if (response.code === 403) {
      // Vue.$vux.toast.text(response.msg, 'top')
      store.commit('logout')
      // 只有在当前路由不是登录页面才跳转
      router.currentRoute.name !== 'login' && router.replace({
        path: 'login',
        query: { redirect: router.currentRoute.fullPath }
      })
    } else {
      return response
    }
  },
  error => {
    if (error.response) {
      console.error(error)
      switch (error.response.status) {
        case 403:
          // 403 清除token信息并跳转到登录页面
          store.commit('logout')
          // 只有在当前路由不是登录页面才跳转
          router.currentRoute.name !== 'login' && router.replace({
            path: 'login',
            query: { redirect: router.currentRoute.fullPath }
          })
          break
        default:
          error.message = `连接错误：${error.response.status}`
      }
    }
    return Promise.reject(error)
  }
)

export default {
  // get请求 form key-value 形式请求 application/x-www-form-urlencoded
  get (url, param, warning) {
    return new Promise((resolve, reject) => {
      Vue.$vux.loading.show({
        text: '加载中'
      })
      axios({
        method: 'get',
        url,
        params: param,
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: [(data, header) => {
          return qs.stringify(data)
        }],
        cancelToken: new CancelToken(c => {
          cancel = c
        })
      }).then(res => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        if (!res) {
          console.error(res)
          return
        }
        if (res.code === 0) {
          resolve(res)
        } else {
          if (warning) {
            resolve(res)
          } else {
            Vue.$vux.toast.text(res.msg)
            resolve(res)
          }
        }
      }).catch(error => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        reject(error)
        if (error.message !== '操作取消') {
          Vue.$vux.toast.show({
            position: 'default',
            time: '3000',
            type: 'warn',
            isShowMask: false,
            text: error.message
          })
        } else {
          Vue.$vux.toast.text('操作取消')
        }
      })
    })
  },
  // post请求 form key-value 形式请求 application/x-www-form-urlencoded
  post (url, param, warning) {
    return new Promise((resolve, reject) => {
      Vue.$vux.loading.show({
        text: '加载中'
      })
      axios({
        method: 'post',
        url,
        data: param,
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: [(data, header) => {
          return qs.stringify(data)
        }],
        cancelToken: new CancelToken(c => {
          cancel = c
        })
      }).then(res => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        if (!res) {
          console.error(res)
          return
        }
        if (res.code === 0) {
          resolve(res)
        } else {
          if (warning) {
            resolve(res)
          } else {
            Vue.$vux.toast.text(res.msg)
            resolve(res)
          }
        }
      }).catch((error) => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        reject(error)
        if (error.message !== '操作取消') {
          Vue.$vux.toast.show({
            position: 'default',
            time: '3000',
            type: 'warn',
            isShowMask: false,
            text: error.message
          })
        } else {
          Vue.$vux.toast.text('操作取消')
        }
      })
    })
  },
  // get请求 JSON 形式请求 application/json
  getJson (url, param, warning) {
    return new Promise((resolve, reject) => {
      Vue.$vux.loading.show({
        text: '加载中'
      })
      axios({
        method: 'get',
        url,
        data: param,
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'application/json'
        },
        transformRequest: [(data, header) => {
          return JSON.stringify(data)
        }],
        cancelToken: new CancelToken(c => {
          cancel = c
        })
      }).then(res => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        if (!res) {
          console.error(res)
          return
        }
        if (res.code === 0) {
          resolve(res)
        } else {
          if (warning) {
            resolve(res)
          } else {
            Vue.$vux.toast.text(res.msg)
            resolve(res)
          }
        }
      }).catch((error) => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        reject(error)
        if (error.message !== '操作取消') {
          Vue.$vux.toast.show({
            position: 'default',
            time: '3000',
            type: 'warn',
            isShowMask: false,
            text: error.message
          })
        } else {
          Vue.$vux.toast.text('操作取消')
        }
      })
    })
  },
  // post请求 JSON 形式请求 application/json
  postJson (url, param, warning) {
    return new Promise((resolve, reject) => {
      Vue.$vux.loading.show({
        text: '加载中'
      })
      axios({
        method: 'post',
        url,
        data: param,
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'application/json;charset=UTF-8'
        },
        transformRequest: [(data, header) => {
          return JSON.stringify(data)
        }],
        cancelToken: new CancelToken(c => {
          cancel = c
        })
      }).then(res => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        if (!res) {
          console.error(res)
          return
        }
        if (res.code === 0) {
          resolve(res)
        } else {
          if (warning) {
            resolve(res)
          } else {
            Vue.$vux.toast.text(res.msg)
            resolve(res)
          }
        }
      }).catch((error) => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        reject(error)
        if (error.message !== '操作取消') {
          Vue.$vux.toast.show({
            position: 'default',
            time: '3000',
            type: 'warn',
            isShowMask: false,
            text: error.message
          })
        } else {
          Vue.$vux.toast.text('操作取消')
        }
      })
    })
  },
  // get请求 form 原生 form-data 形式请求 multipart/form-data
  getForm (url, param, warning) {
    let formData = new FormData()
    for (let item in param) {
      formData.append(item, param[item])
    }
    return new Promise((resolve, reject) => {
      Vue.$vux.loading.show({
        text: '加载中'
      })
      axios({
        method: 'get',
        url,
        data: formData,
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'multipart/form-data'
        },
        cancelToken: new CancelToken(c => {
          cancel = c
        })
      }).then(res => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        if (!res) {
          console.error(res)
          return
        }
        if (res.code === 0) {
          resolve(res)
        } else {
          if (warning) {
            resolve(res)
          } else {
            Vue.$vux.toast.text(res.msg)
            resolve(res)
          }
        }
      }).catch((error) => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        reject(error)
        if (error.message !== '操作取消') {
          Vue.$vux.toast.show({
            position: 'default',
            time: '3000',
            type: 'warn',
            isShowMask: false,
            text: error.message
          })
        } else {
          Vue.$vux.toast.text('操作取消')
        }
      })
    })
  },
  // post请求 form 原生 form-data 形式请求 multipart/form-data
  postForm (url, param, warning) {
    let formData = new FormData()
    for (let item in param) {
      formData.append(item, param[item])
    }
    return new Promise((resolve, reject) => {
      Vue.$vux.loading.show({
        text: '加载中'
      })
      axios({
        method: 'post',
        url,
        data: formData,
        headers: {
          'X-Requested-With': 'XMLHttpRequest',
          'Content-Type': 'multipart/form-data'
        },
        cancelToken: new CancelToken(c => {
          cancel = c
        })
      }).then(res => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        if (!res) {
          console.error(res)
          return
        }
        if (res.code === 0) {
          resolve(res)
        } else {
          if (warning) {
            resolve(res)
          } else {
            Vue.$vux.toast.text(res.msg)
            resolve(res)
          }
        }
      }).catch((error) => {
        if (Vue.$vux.loading) {
          Vue.$vux.loading.hide()
        }
        reject(error)
        if (error.message !== '操作取消') {
          Vue.$vux.toast.show({
            position: 'default',
            time: '3000',
            type: 'warn',
            isShowMask: false,
            text: error.message
          })
        } else {
          Vue.$vux.toast.text('操作取消')
        }
      })
    })
  }
}
