import http from './http'

// 登录
export const getLogin = params => http.get('/web/user/login', params)
// 注册
export const getRegister = params => http.get('/web/user/register', params)
// 忘记密码
export const getForgetPassword = params => http.get('/web/user/forgetPassword', params)
// 获取验证码
export const getFecthChechCode = params => http.post('/web/user/sendPhone', params)
// 获取用户信息
export const getUserInfoById = params => http.get('/web/user/getUserInfoById', params)
// 删除账户
export const getRemoveUse = params => http.get('/web/user/removeUser', params)
// 首页友情链接
export const getSelectLink = params => http.get('/web/index/selectLink', params)
// 首页合作企业
export const getSelectEnterprise = params => http.get('/web/index/selectEnterprise', params)
// 首页推荐职位
export const getSelectPosition = params => http.get('/web/index/selectPosition', params)
// 首页师徒制工作室
export const getSelectStudio = params => http.get('/web/index/selectStudio', params)
// 首页政策解读
export const getSelectPolicy = params => http.get('/web/index/selectPolicy', params)
// 首页新闻公告
export const getSelectNews = params => http.get('/web/index/selectNews', params)
// 首页创意集市
export const getSelectProduct = params => http.get('/web/index/selectProduct', params)
// 首页设计/制作任务
export const getSelectNeeds = params => http.get('/web/index/selectNeeds', params)
// 首页推荐设计师
export const getSelectDesigner = params => http.get('/web/index/selectDesigner', params)
// 首页实战案例
export const getSelectLiterature = params => http.get('/web/index/selectLiterature', params)
// 首页合作高校
export const getSelectSchool = params => http.get('/web/index/selectSchool', params)
// 首页高校图片
export const getSchoolLogo = params => http.get('/web/index/getSchoolLogo', params)
// 首页静态图片
export const getSelectHomePicture = params => http.get('/web/index/selectHomePicture', params)
// 首页轮播图
export const getSelectBanner = params => http.get('/web/index/selectBanner', params)
// 首页新闻公告-重复
// export const getNewsById = params => http.get('/web/simple/getNewsById', params)
// 技能提升-实战案例
export const getSkillSelectLiterature = params => http.get('/web/increaseSkill/selectLiterature', params)
// 技能提升-实战案例-大类详情
export const getSkillSelectLiterByType = params => http.get('/web/increaseSkill/selectLiterByType', params)
// 技能提升-实战案例-小类列表
export const getSkillLiterByType = params => http.get('/web/increaseSkill/getLiterByType', params)
// 技能提升-名师推荐
export const getSkillSelectRecommend = params => http.get('/web/increaseSkill/selectRecommend', params)
// 技能提升-名师推荐tab
export const getSkillSelectClassify = params => http.get('/web/increaseSkill/selectClassify', params)
// 优秀作品
export const getSkillSelectGood = params => http.get('/web/index/selectLiteratureGood', params)

// 技能提升-名师推荐-作品详情
export const getSkillLiterById = params => http.get('/web/increaseSkill/getLiterById', params)
// 技能提升-名师推荐-作品详情-评论列表
export const getSkillGetComment = params => http.get('/web/comment/getComment', params)
// 技能提升-名师推荐-作品详情-判断当前用户是否点赞过作品
export const postSkillIsLike = params => http.post('/web/comment/isLike', params)
// 技能提升-名师推荐-作品详情-点赞作品
export const postSkillLike = params => http.post('/web/comment/like', params)
// 技能提升-名师推荐-作品详情-取消点赞
export const postSkillNotLike = params => http.post('/web/comment/notLike', params)
// 技能提升-名师推荐-作品详情-取消点赞
export const postSkillCancelMessage = params => http.post('/web/creative/cancelMessage', params)
// 技能提升-名师推荐-作品详情-提交评论
export const postSkilladdComment = params => http.post('/web/comment/addComment', params)
// 技能提升-名师推荐-作品详情-同类作品推荐
export const getSkillSameLiter = params => http.get('/web/increaseSkill/getSameLiter', params)
// 技能提升-名师推荐-设计师详情
export const getDesignerInfo = params => http.get('/web/simple/getDesignerInfo', params)
// 设计师详情-关注/取消关注
export const getAddAttention = params => http.get('/web/comment/addAttention', params)
export const getDelAttention = params => http.get('/web/comment/delAttention', params)
// 关注列表
export const getAttentionList = params => http.get('/web/user/getAttention', params)
// 是否关注
export const getIsAttention = params => http.get('/web/comment/isAttention', params)
// 技能提升-名师推荐-设计师作品列表
export const getDesignerLiter = params => http.get('/web/simple/getDesignerLiter', params)
// 技能提升-名师推荐-课程列表
export const getCourseByCreateId = params => http.get('/web/increaseSkill/getCourseByCreateId', params)
// 技能提升-名师推荐-成就列表-重复
// export const getAchieve = params => http.get('/web/user/getAchieve', params)
// 技能提升-名师推荐-同类设计师作品推荐
export const getSameDesigner = params => http.get('/web/simple/getSameDesigner', params)
// 技能提升-实战课堂 课程类别 一级分类
export const getSkillOneClassify = params => http.get('/web/increaseSkill/getOneClassify', params)
// 技能提升-实战课堂 课程主题 二级分类
export const getSkillTwoClassify = params => http.get('/web/increaseSkill/getTwoClassify', params)
// 技能提升-实战课堂-最新课程
export const getSkillNewCourse = params => http.get('/web/increaseSkill/getNewCourse', params)
// 技能提升-实战课堂-系列好课
export const getSkillGoodCourse = params => http.get('/web/increaseSkill/getGoodCourse', params)
// 技能提升-实战课堂-低价好课
export const getSkillCheapCourse = params => http.get('/web/increaseSkill/getCheapCourse', params)
// 技能提升-实战课堂-课程详情
export const getSkillOneCourse = params => http.get('/web/increaseSkill/getOneCourse', params)
// 技能提升-实战课堂-检查立刻报名
export const getSkillJoinCourseStatus = params => http.get('/web/increaseSkill/joinCourseStatus', params)
// 技能提升-实战课堂-立刻报名
export const getSkillJoinCourse = params => http.get('/web/increaseSkill/joinCourse', params)
// 技能提升-实战课堂-课程详情-推荐课程
export const getSkillSameCourse = params => http.get('/web/increaseSkill/getSameCourse', params)
// 创意集市-创意产品-右侧分类
export const getProductClassifyList = params => http.get('/web/creative/getProductClassifyList', params)
// 创意集市-创意产品-根据分类查询创意产品
export const getProductList = params => http.get('/web/creative/getProductList', params)
// 创意集市-创意产品-产品查看
export const getProduct = params => http.get('/web/simple/getProduct', params)
// 创意集市-创意产品-检查收藏
export const postIsCollect = params => http.post('/web/comment/isCollect', params)
// 创意集市-创意产品-收藏
export const postCollect = params => http.post('/web/comment/collect', params)
// 创意集市-创意产品-取消收藏
export const postNotCollect = params => http.post('/web/comment/notCollect', params)
// 创意集市-创意产品-同类产品推荐
export const getSameProduct = params => http.get('/web/simple/getSameProduct', params)
// 创意集市-创意产品-站内信
export const postSendMessage = params => http.post('/web/creative/sendMessage', params)
// 创意集市-设计|制作任务列表
export const getNeedsList = params => http.get('/web/creative/getNeedsList', params)
// 创意集市-设计|制作任务-单个任务查看详情
export const getOneNeeds = params => http.get('/web/creative/getOneNeeds', params)
// 创意集市-设计|制作任务-判断任务状态
export const getNeedsStatus = params => http.get('/web/creative/getNeedsStatus', params)
// 创意集市-设计|制作任务-用户领取任务
export const getAcceptNeeds = params => http.get('/web/creative/acceptNeeds', params)
// 创意集市-设计|制作任务-用户提交任务
export const getSubmitNeeds = params => http.post('/web/creative/submitNeeds', params)
// 创意集市-设计|制作任务-同类任务推荐
export const getSameNeeds = params => http.get('/web/creative/getSameNeeds', params)
// 校企合作-师徒制工作室列表
export const getStudioList = params => http.get('/web/partner/getStudioList', params)
// 校企合作-师徒制工作室-工作室信息
export const getStudioInfo = params => http.get('/web/simple/getStudioInfo', params)
// 校企合作-师徒制工作室-判断是否加入工作室
export const getJoinStudioStatus = params => http.get('/web/simple/joinStudioStatus', params)
// 校企合作-师徒制工作室-加入工作室
export const getJoinStudio = params => http.get('/web/simple/joinStudio', params)
// 校企合作-师徒制工作室-人员列表
export const getStudioUser = params => http.get('/web/simple/getStudioUser', params)
// 校企合作-师徒制工作室-参赛作品
export const getStudioWork = params => http.get('/web/simple/getStudioWork', params)
// 校企合作-师徒制工作室-获奖作品
export const getPrizeWork = params => http.get('/web/simple/getPrizeWork', params)
// 校企合作-师徒制工作室-参赛作品-作品详情
export const getOneStudioWork = params => http.get('/web/simple/getOneStudioWork', params)
// 校企合作-师徒制工作室-信息公告列表
export const getStudioNews = params => http.get('/web/simple/getStudioNews', params)
// 校企合作-师徒制工作室-信息公告-详情
export const getOneNews = params => http.get('/web/simple/getOneNews', params)
// 校企合作-创意机构 
export const getEnterpriseList = params => http.get('/web/partner/getEnterpriseList', params)
// 校企合作-创意机构-企业信息
export const getEnterprise = params => http.get('/web/simple/getEnterprise', params)
// 校企合作-创意机构-企业人员列表
export const getEnterpriseUser = params => http.get('/web/simple/getEnterpriseUser', params)
// 校企合作-创意机构-企业作品列表
export const getEnterpriseWork = params => http.get('/web/simple/getEnterpriseWork', params)
// 校企合作-创意机构-同类企业推荐
export const getSameEnterprise = params => http.get('/web/simple/getSameEnterprise', params)
// 校企合作-创意机构-成就列表
export const getEnterAchieve = params => http.get('/web/simple/getEnterAchieve', params)
// 校企合作-创意机构-工作列表
export const getJobListByUserId = params => http.get('/web/job/getJobListByUserId', params)
// 校企合作-创业园区
export const getParkList = params => http.get('/web/partner/getParkList', params)
// 校企合作-创业园区-园区详情
export const getParkById = params => http.get('/web/partner/getParkById', params)
// 校企合作-创业园区-园区政策
export const getPolicyNews = params => http.get('/web/partner/getPolicyNews', params)
// 校企合作-创业园区-园区新闻公告
export const getParkNews = params => http.get('/web/partner/getParkNews', params)
// 校企合作-创业园区-园区创新团队
export const getJoinEnter = params => http.get('/web/partner/getJoinEnter', params)
// 校企合作-创业园区-园区优秀团队
export const getGoodEnter = params => http.get('/web/partner/getGoodEnter', params)
// 技能大赛-轮播图
export const getBannerList = params => http.get('/web/skill/getBannerList', params)
// 技能大赛-大赛新闻banner
export const getNewsBanner = params => http.get('/web/skill/getNewsBanner', params)
// 技能大赛-大赛新闻列表
export const getNews = params => http.get('/web/skill/getNews', params)
// 技能大赛-大赛新闻-新闻详情
export const getNewsById = params => http.get('/web/simple/getNewsById', params)
// 技能大赛-获奖作品分析
export const getAwardList = params => http.get('/web/skill/getAwardList', params)
// 技能大赛-获奖作品分析-详情
export const getOneAward = params => http.get('/web/skill/getOneAward', params)
// 技能大赛-大赛工作室列表
export const getGameStudio = params => http.get('/web/skill/getGameStudio', params)
// 技能大赛-获奖作品分析-更多-大赛名称下拉框
export const getSkillStudioList = params => http.get('/web/skill/getStudioList', params)
// 技能大赛-获奖作品分析-更多-分析条件查询
export const getSelectAwardListMore = params => http.get('/web/skill/selectAwardListMore', params)
// 就业信息-就业资讯列表
export const getJobNews = params => http.get('/web/job/getJobNews', params)
// 就业信息-就业资讯列表-就业咨询详情
export const getOneJobNews = params => http.get('/web/job/getOneJobNews', params)
// 就业信息-高端职位列表
export const getHighJob = params => http.get('/web/job/getHighJob', params)
// 职位列表搜索
// export const getSearchJob = params => http.post('/web/job/search', params)
// 就业信息-高端职位列表-高端职位详情
export const getOneJob = params => http.get('/web/job/getOneJob', params)
// 就业信息-企业招聘信息
export const getPositionsByEntId = params => http.get('/web/mart/getPositionsByEntId', params)
// 就业信息-高端职位列表-高端职位详情-其他职位
export const getSameJob = params => http.get('/web/job/getSameJob', params)
// 对设计师-师徒工作室-企业 发私信
export const PostPrivateLetter = params => http.post('/web/creative/sendMessage', params)
// 就业信息-常驻企业列表
export const getResidentEnter = params => http.get('/web/job/getResidentEnter', params)
// 个人中心-我的消息
export const getMessage = params => http.get('/web/user/getMessage', params)
// 个人中心-账号安全-修改邮箱
export const getUpdateEmail = params => http.get('/web/user/updateEmail', params)
// 个人中心-账号安全-修改手机
export const getUpdatePhone = params => http.get('/web/user/updatePhone', params)
// 个人中心-账号安全-修改密码
export const getUpdatePassword = params => http.get('/web/user/updatePassword', params)
// 个人中心-提交我的资料
export const postUpdateUser = params => http.postForm('/web/user/updateUser', params)
// 个人中心-获取省列表
export const getProvince = params => http.get('/web/area/getProvince', params)
// 个人中心-获取市列表
export const getCity = params => http.get('/web/area/getCity', params)
// 个人中心-获取区列表
export const getArea = params => http.get('/web/area/getArea', params)
// 个人中心-我的作品
export const getLiterByUserId = params => http.get('/web/user/getLiterByUserId', params)
// 个人中心-我的作品-发布作品-一级分类列表-重复
// export const getSelectClassify = params => http.get('/web/increaseSkill/selectClassify', params)
// 个人中心-我的作品-发布作品-二级分类列表
export const getClassByParent = params => http.get('/web/increaseSkill/getClassByParent', params)
// 个人中心-我的作品-添加作品
export const postAddLiter = params => http.postForm('/web/user/addLiter', params)
// 个人中心-我的评论
export const getComment = params => http.get('/web/user/getComment', params)
// 个人中心-我的收藏
export const getCollect = params => http.get('/web/user/getCollect', params)
// 个人中心-我的成就
export const getAchieve = params => http.get('/web/user/getAchieve', params)
// 个人中心-我的成就-添加成就
export const postAddAchieve = params => http.post('/web/user/addAchieve', params)
// 个人中心-我的成就-删除成就
export const postdelAchieve = params => http.post('/web/user/delAchieve', params)
// 个人中心-我的任务
export const getNeeds = params => http.get('/web/user/getNeeds', params)
// 个人中心-我的任务-发布任务
export const postAddNeeds = params => http.post('/web/user/addNeeds', params)
// 个人中心-我的认证-获取认证状态
export const getApprove = params => http.get('/web/user/getApprove', params)
// 个人中心-我的认证-设计师认证
export const postApproveDesigner = params => http.post('/web/approve/approveDesigner', params)
// 个人中心-我的认证-设计师认证-学校列表
export const getSchoolList = params => http.get('/web/approve/getSchoolList', params)
// 个人中心-我的认证-设计师认证-擅长领域
export const getGoodsAt = params => http.get('/web/approve/getGoodsAt', params)
// 个人中心-我的认证-企业认证
export const postApproveEnterprise = params => http.post('/web/approve/approveEnterprise', params)
// 个人中心-我的认证-企业认证-擅长领域
export const getEnterAt = params => http.get('/web/approve/getEnterAt', params)
// 个人中心-我的认证-园区认证
export const postApprovePark = params => http.post('/web/approve/approvePark', params)
// 校企合作-创新创业-优品汇列表
export const getInnovateGoods = params => http.get('/web/innovate/getInnovateGoods', params)
// 校企合作-创新创业-优品汇列表-详情
export const getOneGoods = params => http.get('/web/innovate/getOneGoods', params)
// 校企合作-创新创业-创业梦工厂
export const getDreamFactory = params => http.get('/web/innovate/getDreamFactory', params)
// 校企合作-创新创业-创业梦工厂-详情
export const getOneDreamFactory = params => http.get('/web/innovate/getOneDreamFactory', params)
// 校企合作-创新创业-创业天使团
export const getAngelTroupe = params => http.get('/web/innovate/getAngelTroupe', params)
// 校企合作-创新创业-创业天使团-详情
export const getOneTroupe = params => http.get('/web/innovate/getOneTroupe', params)
// 校企合作-创新创业-创业导师列表
export const getTutor = params => http.get('/web/innovate/getTutor', params)
// 校企合作-创新创业-创业导师列表-详情
export const getOneTutor = params => http.get('/web/innovate/getOneTutor', params)
// 校企合作-创新创业-创业课程列表
export const getCurriculum = params => http.get('/web/innovate/getCurriculum', params)
// 校企合作-创新创业-创业课程列表-详情
export const getOneCurriculum = params => http.get('/web/innovate/getOneCurriculum', params)
// 个人中心-我的任务-删除任务
export const getRemoveTask = params => http.post('/web/needs/remove', params)
// 个人中心-我的任务-修改任务
export const getEditTask = params => http.post('/web/needs/edit', params)
// 个人中心-我的任务-查看方案
// export const getLookTask = params => http.post('/web/needsScheme/list', params)
export const getLookTask = params => http.post('/web/Collect/toviewPlan', params)
// 添加收藏
export const postAddCollect = params => http.post('/web/Collect/addCollect', params)
// 取消收藏
export const postDelCollect = params => http.post('/web/Collect/removeCollect', params)
// 是否收藏
export const getIsCollect = params => http.post('/web/Collect/isCollect', params)
