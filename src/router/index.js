import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  mode: 'hash',
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'layout',
      component: resolve => require(['../pages/layout'], resolve),
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'home',
          component: resolve => require(['../pages/home'], resolve),
          // meta: { title: '首页', keepAlive: true, routerZIndex: 1 }
          meta: { title: '推荐高校', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/user',
          name: 'user',
          component: resolve => require(['../pages/user'], resolve),
          meta: { title: '个人中心', keepAlive: false, routerZIndex: 1 },
          redirect: '/userMaterial',
          children: [{
            path: '/userMaterial',
            name: 'userMaterial',
            component: resolve => require(['../pages/user/userMaterial'], resolve),
            meta: { title: '我的资料', keepAlive: false, routerZIndex: 1 }
          }, {
            path: '/userMessage',
            name: 'userMessage',
            component: resolve => require(['../pages/user/userMessage'], resolve),
            meta: { title: '我的消息', keepAlive: false, routerZIndex: 1 }
          }, {
            path: '/userSecure',
            name: 'userSecure',
            component: resolve => require(['../pages/user/userSecure'], resolve),
            meta: { title: '账号安全', keepAlive: false, routerZIndex: 1 }
          }, {
            path: '/userWorks',
            name: 'userWorks',
            component: resolve => require(['../pages/user/works'], resolve),
            meta: { title: '我的作品', keepAlive: false, routerZIndex: 1 }
          }, {
            path: '/userApprove',
            name: 'userApprove',
            component: resolve => require(['../pages/user/userApprove'], resolve),
            meta: { title: '我的认证', keepAlive: false, routerZIndex: 1 }
          }, {
            path: '/userAppraise',
            name: 'userAppraise',
            component: resolve => require(['../pages/user/userAppraise'], resolve),
            meta: { title: '我的评论', keepAlive: false, routerZIndex: 1 }
          }, {
            path: '/userGaze',
            name: 'userGaze',
            component: resolve => require(['../pages/user/userGaze'], resolve),
            meta: { title: '我的关注', keepAlive: false, routerZIndex: 1 }
          }, {
            path: '/userCollect',
            name: 'userCollect',
            component: resolve => require(['../pages/user/userCollect'], resolve),
            meta: { title: '我的收藏', keepAlive: false, routerZIndex: 1 }
          }, {
            path: '/userAchievement',
            name: 'userAchievement',
            component: resolve => require(['../pages/user/userAchievement'], resolve),
            meta: { title: '我的成就', keepAlive: false, routerZIndex: 1 }
          }, {
            path: '/userTask',
            name: 'userTask',
            component: resolve => require(['../pages/user/userTask'], resolve),
            meta: { title: '我的任务', keepAlive: false, routerZIndex: 1 }
          }]
        },
        {
          path: '/skillLift',
          name: 'skillLift',
          component: resolve => require(['../pages/skillLift'], resolve),
          meta: { title: '技能提升', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/ideaMarket',
          name: 'ideaMarket',
          component: resolve => require(['../pages/ideaMarket'], resolve),
          meta: { title: '创意集市', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/teamwork',
          name: 'teamwork',
          component: resolve => require(['../pages/teamwork'], resolve),
          meta: { title: '校企合作', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/megagame',
          name: 'megagame',
          component: resolve => require(['../pages/megagame'], resolve),
          meta: { title: '技能大赛', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/recruiting',
          name: 'recruiting',
          component: resolve => require(['../pages/recruiting'], resolve),
          meta: { title: '技能大赛', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/teacherRecommend',
          name: 'teacherRecommend',
          component: resolve => require(['../pages/skillLift/teacherRecommend'], resolve),
          meta: { title: '名师推荐', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/practicalClassroom',
          name: 'practicalClassroom',
          component: resolve => require(['../pages/skillLift/practicalClassroom'], resolve),
          meta: { title: '实战课堂', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/courseDetail',
          name: 'courseDetail',
          component: resolve => require(['../pages/skillLift/courseDetail'], resolve),
          meta: { title: '课程详情', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/excellentWorks',
          name: 'excellentWorks',
          component: resolve => require(['../pages/home/excellentWorks'], resolve),
          meta: { title: '优秀作品', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/jobVacancy',
          name: 'jobVacancy',
          component: resolve => require(['../pages/recruiting/jobVacancy'], resolve),
          meta: { title: '招聘职位', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/designTask',
          name: 'designTask',
          component: resolve => require(['../pages/ideaMarket/designTask'], resolve),
          meta: { title: '设计|制作任务', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/designTaskDetail',
          name: 'designTaskDetail',
          component: resolve => require(['../pages/ideaMarket/designTaskDetail'], resolve),
          meta: { title: '设计|制作任务详情', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/worksDetail',
          name: 'worksDetail',
          component: resolve => require(['../pages/user/works/worksDetail'], resolve),
          meta: { title: '作品详情', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/newsList',
          name: 'newsList',
          component: resolve => require(['../pages/megagame/newsList'], resolve),
          meta: { title: '新闻列表|大赛新闻', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/newsDetail',
          name: 'newsDetail',
          component: resolve => require(['../pages/megagame/newsDetail'], resolve),
          meta: { title: '新闻详情|大赛新闻详情', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/stylistInfo',
          name: 'stylistInfo',
          component: resolve => require(['../pages/teamwork/stylistInfo'], resolve),
          meta: { title: '设计师信息|师徒值工作室|大赛工作室|企业', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/stylistInfoList',
          name: 'stylistInfoList',
          component: resolve => require(['../pages/teamwork/stylistInfoList'], resolve),
          meta: { title: '设计师信息|师徒值工作室|大赛工作室|企业', keepAlive: false, routerZIndex: 1 }
        },
        {
          path: '/schoolyard',
          name: 'schoolyard',
          component: resolve => require(['../pages/teamwork/schoolyard'], resolve),
          meta: { title: '园区详情', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/tutorDetail',
          name: 'tutorDetail',
          component: resolve => require(['../pages/megagame/tutorDetail'], resolve),
          meta: { title: '导师列表', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/recruitJob',
          name: 'recruitJob',
          component: resolve => require(['../pages/home/recruitJob'], resolve),
          meta: { title: '职位列表', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/worksLiat',
          name: 'worksLiat',
          component: resolve => require(['../pages/user/works/worksLiat'], resolve),
          meta: { title: '作品列表', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/courseList',
          name: 'courseList',
          component: resolve => require(['../pages/skillLift/courseList'], resolve),
          meta: { title: '课程列表', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/protocol',
          name: 'protocol',
          component: resolve => require(['../pages/login/protocol'], resolve),
          meta: { title: '用户注册协议', keepAlive: true, routerZIndex: 1 }
        },
        {
          path: '/classifyList',
          name: 'classifyList',
          component: resolve => require(['../pages/skillLift/classifyList'], resolve),
          meta: { title: '分类列表', keepAlive: true, routerZIndex: 1 }
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: resolve => require(['../pages/login/login'], resolve),
      meta: { title: '登录' }
    },
    {
      path: '/register',
      name: 'register',
      component: resolve => require(['../pages/login/register'], resolve),
      meta: { title: '注册' }
    },
    {
      path: '/forget',
      name: 'forget',
      component: resolve => require(['../pages/login/forget'], resolve),
      meta: { title: '忘记密码' }
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    this.app.$store.commit('pushScroll', {
      pathName: from.name,
      scrollTop: document.getElementById('vux_view_box_body').scrollTop,
      scrollLeft: document.getElementById('vux_view_box_body').scrollLeft
    })
    let mark = this.app.$store.state.global.globalScrollMark
    if (mark[to.name]) {
      document.getElementById('vux_view_box_body').scrollTop = mark[to.name].scrollTop
      document.getElementById('vux_view_box_body').scrollLeft = mark[to.name].scrollLeft
    } else {
      document.getElementById('vux_view_box_body').scrollTop = 0
      document.getElementById('vux_view_box_body').scrollLeft = 0
    }
  }
})

export default router
